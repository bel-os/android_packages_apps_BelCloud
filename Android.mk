LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
 
# Name of the APK to build
LOCAL_MODULE := Cloud
LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := Cloud

reversion		:= 30070000
cloud_root	:= $(LOCAL_PATH)
cloud_out	:= $(PWD)/$(OUT_DIR)/target/common/obj/APPS/$(LOCAL_MODULE)_intermediates
cloud_build	:= $(cloud_root)/build
cloud_apk	:= build/outputs/apk/generic/release/generic-release-$(reversion).apk

$(cloud_root)/$(cloud_dir)/$(cloud_apk):
	cd $(cloud_root) && rm -rf build
	cd $(cloud_root) && ./gradlew assemblegeneric

# Build all java files in the java subdirectory
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := $(cloud_apk)
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
 
# Tell it to build an APK
include $(BUILD_PREBUILT)
